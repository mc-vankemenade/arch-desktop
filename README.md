# Arch linux desktop configuration
The goal of this repo is to create my personal perfect desktop environment for arch based distributions.

## Packages
### Package Managers
- Pacman
- yay
- flatpak
### Desktop Environment
- Display Manager: https://github.com/fairyglade/ly
- Window Manager: i3-gaps
- Background: nitrogen
- compositor: picom
- Lock Screen: https://github.com/owenthewizard/i3lockr 
- Status Bar: Polybar
- App Launcher: Rofi, https://github.com/adi1090x/rofi
- Noto Fonts
- gtk3
- gruvbox-dark-gtk (yay)
- gruvbox-icon-theme (yay)

### Networking (wifi)
- nmtui
- networkmanager-applet

### Audio
- pipewire
- pipewire-pulse
- pavucontrol
- cava (yay)

### Bluetooth
- bluez
- blueberry

### System Tools
- Text Editor: https://micro-editor.github.io/index.html
- File Manager: Dolphin
- Note app: https://github.com/zadam/trilium/wiki/
- Browser: Ungoogled Chromium (flatpak)
- Development Environment: VSCodium
- Shell: Fish
- Media player: VLC media player
